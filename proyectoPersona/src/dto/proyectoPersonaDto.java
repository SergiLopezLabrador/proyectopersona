package dto;

public class proyectoPersonaDto {
	//Aqu� creo las variables tipo private para configurar los constructores
	private String nombre;
	private int edad;
	private String dni;
	final private char SEXO;
	private double peso;
	private double altura;
	
	
	//Aqu� creo un constructor default
	public proyectoPersonaDto() {
		
		this.nombre = "";
		this.edad = 0;
		this.dni = "39955523S";
		this.SEXO = 'H';
		this.peso = 0.0;
		this.altura = 0.0;
		
	}

	//Aqu� creo un constructor con todos los atributos
	public proyectoPersonaDto(String nombre, int edad, String dni, char sexo, double peso, double altura) {
		super();
		this.nombre = nombre;
		this.edad = edad;
		this.dni = dni;
		this.SEXO = sexo;
		this.peso = peso;
		this.altura = altura;

	}

	//Aqu� convierto los constructores a string para que al Main salga bien visualmente
	@Override
	public String toString() {
		return "proyectoPersonaDto [nombre=" + nombre + ", edad=" + edad + ", dni=" + dni + ", SEXO=" + SEXO + ", peso="
				+ peso + ", altura=" + altura + "]";
	}
	
	
	
	
	

	

}
